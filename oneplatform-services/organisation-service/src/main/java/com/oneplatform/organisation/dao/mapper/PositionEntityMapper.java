package com.oneplatform.organisation.dao.mapper;

import com.oneplatform.base.dao.CustomBaseMapper;
import com.oneplatform.organisation.dao.entity.PositionEntity;

public interface PositionEntityMapper extends CustomBaseMapper<PositionEntity> {
}