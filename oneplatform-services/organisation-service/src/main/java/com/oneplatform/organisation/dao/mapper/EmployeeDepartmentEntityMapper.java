package com.oneplatform.organisation.dao.mapper;

import com.oneplatform.base.dao.CustomBaseMapper;
import com.oneplatform.organisation.dao.entity.EmployeeDepartmentEntity;

public interface EmployeeDepartmentEntityMapper extends CustomBaseMapper<EmployeeDepartmentEntity> {
}